package com.vsevolod.taskmanager.Constants;

public class Constants {
    public static final String HELP = "help";
    public static final String ABOUT = "about";
    public static final String VERSION = "version";
}

