package com.vsevolod.taskmanager;

import static com.vsevolod.taskmanager.Selection.Selection.parseArgs;

public class Main {

    public static void main(String[] args) {
        System.out.println("***TASK MANAGER v1.0.0***");
        parseArgs(args);
    }
}
