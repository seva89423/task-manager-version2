package com.vsevolod.taskmanager.Menu;

import com.vsevolod.taskmanager.Constants.Constants;

public class Menu {
    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Constants.HELP + " - Show developer info");
        System.out.println(Constants.VERSION + " - Show version info");
        System.out.println(Constants.ABOUT + " - Show display commands");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }
}
