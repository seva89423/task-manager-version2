package com.vsevolod.taskmanager.Selection;

import com.vsevolod.taskmanager.Constants.Constants;

import static com.vsevolod.taskmanager.Menu.Menu.*;

public class Selection {
    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        if (Constants.HELP.equals(arg)) showHelp();
        if (Constants.ABOUT.equals(arg)) showAbout();
        if (Constants.VERSION.equals(arg)) showVersion();
    }
}
